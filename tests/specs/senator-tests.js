/**
 * @venus-library mocha
 * @venus-include ../../Senator.js
 * @venus-include ../lib/zepto.js
 * @venus-fixture ../fixtures/senator-tests.fixture.html
 */
describe('Initial tests for Senator', function () {
  var sen = new Senator({});

  it ('Should return an instance of Senator', function () {
    expect(sen instanceof Senator).to.equal(true);
  });

  it ('Should fire callback for li (tag)', function () {
    var callback = sinon.spy();
    sen.attach('click', 'li', callback);
    $('li').trigger('click');
    expect(callback.called).to.equal(true);
  });

  it ('Should add event key to attachedEvents object', function () {
    var callback = sinon.spy();
    sen.attach('click', '#container', callback);
    expect('click' in sen.attachedEvents).to.equal(true);
  });

  it ('Should not overwrite current values contained in the event key within the attachedEvent Object', function () {
    var callback = sinon.spy();
    sen.attach('click', '.list', callback);
    expect('#container' in sen.attachedEvents['click']).to.equal(true);
    expect('.list' in sen.attachedEvents['click']).to.equal(true);
  });

  it ('Should fire callback for .list (class)', function () {
    var callback = sinon.spy();
    sen.attach('click', '.list', callback);
    $('.list').trigger('click');
    expect(callback.called).to.equal(true);
  });

  it ('Should fire callback for #container (id)', function () {
    var callback = sinon.spy();
    sen.attach('click', '#container', callback);
    $('#container').trigger('click');
    expect(callback.called).to.equal(true);
  });

  it ('Should add new event key for more than one event type', function () {
    var callback = sinon.spy();
    sen.attach('hover', '.list', callback);
    expect('hover' in sen.attachedEvents).to.equal(true);
    expect('click' in sen.attachedEvents).to.equal(true);
  });

  it ('Should not call globalCallback when an event is fired for an unrelated element.', function () {
    var callback = sinon.spy();
    $('form').trigger('click');
    expect(callback.called).to.equal(false);
  });

  it ('Should respect e.preventDefault() when called from specified callback', function () {
    var callback =  function (e) { e.preventDefault(); };
        e = {
          type: 'submit',
          preventDefault: sinon.spy()
        };
    sen.attach('submit', 'form', callback);
    $('form').trigger(e);
    expect(e.preventDefault.called).to.equal(true);
  });

});