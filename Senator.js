/**
 *   Senator.js
 *   Creator: Doug Wallace
 *   Date: 7/2/2013
 *   Description: Senator.js is a JavaScript event delegation library.
 */

/**
 * Create a new function from the provided <code>fn</code>, change <code>this</code> to the provided scope, optionally
 * overrides arguments for the call. (Defaults to the arguments passed by the caller)
 *
 * @param {Function} fn The function to delegate.
 * @param {Object} scope (optional) The scope (<code><b>this</b></code> reference) in which the function is executed.
 * <b>If omitted, defaults to the browser window.</b>
 * @param {Array} args (optional) Overrides arguments for the call. (Defaults to the arguments passed by the caller)
 * @param {Boolean/Number} appendArgs (optional) if True args are appended to call args instead of overriding,
 * if a number the args are inserted at the specified position
 * @return {Function} The new function
 */
function bind(fn, scope, args, appendArgs) {
  var method = fn,
      applyArgs;

  return function() {
    var callArgs = args || arguments;

    if (appendArgs === true) {
      callArgs = Array.prototype.slice.call(arguments, 0);
      callArgs = callArgs.concat(args);
    }
    else if (typeof appendArgs == 'number') {
      callArgs = Array.prototype.slice.call(arguments, 0); // copy arguments first
      applyArgs = [appendArgs, 0].concat(args); // create method call params
      Array.prototype.splice.apply(callArgs, applyArgs); // splice them in
    }

    return method.apply(scope || window, callArgs);
  };
}

/**
 *  Senator Constructor
 *  @constructor
 *  @desc Creates an instance of Senator (JS Event Delegation Library)
 *  @param {object} config params
 *  @return {Function}
 */
function Senator(config) {
  //Check for instance of Senator - then create a new one
  if (!(this instanceof Senator)) {
    this.config = config || false;
    return new Senator(config);
  }
}

/**
 * attachedEvents
 * @desc Object containing Senator events that have been attached to dom elements
 * @type {object}
 */
Senator.prototype.attachedEvents = {};

/**
 * globalCallback
 * @desc Callback for all events
 * @param {object} event Event data from eventListener
 */
Senator.prototype.globalCallback = function (event) {
  var e = event || window.event,
      type = e.type,
      target = e.target || e.srcElement,
      classArr = target.className.length > 0 ?  target.className.split(' ') : false,
      id = target.id.length > 0 ? '#'+target.id : false,
      attachedEventsType = this.attachedEvents[type],
      i,
      className;

  if(id && id in attachedEventsType) {
    attachedEventsType[id].callback.call(this, e);
  } else if (classArr){
    for (i = 0; i < classArr.length; i++) {
      className = '.' + classArr[i];
      if (className in attachedEventsType) {
        attachedEventsType[className].callback.call(this, e);
      }
    }
  } else if (target.tagName.toLowerCase() in attachedEventsType) {
    attachedEventsType[target.tagName.toLowerCase()].callback.call(this, e);
  }
};

/**
 * attach
 * @desc Attaches an event to the specified element
 * @param {string} selector The selector the eventListener will fire a callback for.
 * @param {string} etype Valid event type
 * @param {method} callback Callback method for the eventListener
 */
Senator.prototype.attach = function (etype, selector, callback) {
  if (etype in this.attachedEvents) {
    this.attachedEvents[etype][selector] = {'callback': callback};
  }else {
    document.addEventListener(etype, bind(this.globalCallback, this, null, false), false);
    this.attachedEvents[etype] = {};
    this.attachedEvents[etype][selector] = {'callback': callback};
  }
};

